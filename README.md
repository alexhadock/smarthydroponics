# Introduction

This project - attempt to create effective management system a hydroponics on Arduino. I decided to use also and ESP8266 to have remote access. I will describe all schemes and the code most clear as far as it is possible in case somebody will want to repeat this project.
The project is subject to free copying and distribution.


# SmartHydroponics componentns list

Board -- **Arduino Leonardo**

Sensor:
 * Temperature ad humidity sensor DHT11 (Analog)
 * Temperature sensor DS18B20 (Analog)
 * Ligth level sensor LM393 (Analog)

 Modules:
 * Relay
 * Aquarian pomp
 * Display 1602 with I2C adapter
 * ESP8266 ESP-01 
 * RGB LED (Common anode)
 * Fluorescent lamp
 ![Scheme](https://i.imgur.com/JHOY1P2.png)

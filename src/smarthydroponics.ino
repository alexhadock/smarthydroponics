//  connect libraies

#include "DHT.h"
#include "OneWire.h"
#include "Wire.h"
#include "LiquidCrystal_I2C.h"
#include "Adafruit_Sensor.h"

//  declaration of pins

#define dht11 1   //  pin of the temperature sensor and air humidity
#define ds18b20 2 //  pin of the soluiton temperature sensor
#define pinR 12  //  pin of the red color for the state indicator
#define pinG 10  //  pin of the green color for the state indicator
#define pinB 9 //  pin of the blue color for the state indicator
#define waterSensor A1  // pin of the water level sensor
#define lightSensor A0  //  pin of the light level sensor
#define pumpRelay 5 // pin of the relay for the pump
#define lampRelay 6 // pin of the relay for the lamp
#define timerStart 0    // 1 - отсчёт периода с момента ВЫКЛЮЧЕНИЯ лампочки, 0 - с ВКЛЮЧЕНИЯ

//  declaration of variables

int temperature;
int humidity;
int lightLvl;
int solutionTemp;
int waterLvl;
int DS;


LiquidCrystal_I2C lcd(0x27,16,2);   // declaration of lcd display

//DHT dht(DHTPIN, DHT22); // uncomment this line if you use the DHT22 sensor
DHT dht(dht11, DHT11);
OneWire  ds1(DS);

unsigned long periodTime = (long)54000000;
unsigned long workTime = 32400000;

unsigned long periodTimer, workTimer;
boolean workFlag;

void normalModeLed() {
  // the LED will burn with green
  digitalWrite(pinR, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinG, HIGH);
}

void economyModeLed() {
  // the LED will burn with blue
  digitalWrite(pinR, LOW);
  digitalWrite(pinG, LOW);
  digitalWrite(pinB, HIGH);
}
  // the LED will burn with red, blinking
void recoveryModeLed() {
  digitalWrite(pinR, HIGH);
  delay(750);
  digitalWrite(pinR, LOW);
  delay(750);
}

void lightSensorWork() {
  lightLvl = analogRead(lightSensor);
  printIllumLvl();
}

// function for measure

void tempAndHumidSensorWork() {
  float humidity = dht.readHumidity(); // measure humidity
  float temperature = dht.readTemperature(); // meuasure temperature

  if (isnan(humidity) || isnan(temperature)) {  // if it does not turn out to consider indications, the error will be displayed
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Reading error!");
    recoveryModeLed();
    return;
  }
  printTemp();
  printHumid();
}

void solutionTempSensorWork() {
  byte data1[2];

  ds1.reset();
  ds1.write(0xCC);
  ds1.write(0x44);

  // read data from ds18b20
  ds1.reset();
  ds1.write(0xCC);
  ds1.write(0xBE);
  data1[0] = ds1.read();
  data1[1] = ds1.read();

  solutionTemp = ((data1[1] << 8) | data1[0]) * 0.0625;

  if (solutionTemp > 26) {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Solution too hot!");
    recoveryModeLed();
    return;
  }

  if (isnan(solutionTemp)) {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Reading error!");
    recoveryModeLed();
    return;
  }

  printSolutionTemp();
}

void waterLevelSensorWork() {
  waterLvl = analogRead(waterSensor);
}

void userGreeting() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("SmartHydroponics");
  lcd.setCursor(0,1);
  lcd.print("@alexhadock");
}

void pumpWork() {
  digitalWrite(pumpRelay, HIGH);
  printPumpWork();
  if (waterLvl > 300) {
    digitalWrite(pumpRelay, LOW);

  }
}

// function for print temperature of air
void printTemp() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Temperature");
  lcd.setCursor(0,1);
  lcd.print(temperature);
  lcd.print("*C");
}

// function for print humidity
void printHumid() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Humidity");
  lcd.setCursor(0,1);
  lcd.print(humidity);
  lcd.print(" %\t");
}

void printSolutionTemp() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Solution temp.");
  lcd.setCursor(0,1);
  lcd.print(solutionTemp);
  lcd.print("*C");
}

void printIllumLvl() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Illumination level =");
  lcd.print(lightLvl);
}

void printPumpWork() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Pump is work");
  lcd.setCursor(0,1);
  lcd.print("Liquid level = ");
  lcd.print(waterLvl);
}

void setup() {
  periodTimer = millis();
  pinMode(pinR, OUTPUT);
  pinMode(pinG, OUTPUT);
  pinMode(pinB, OUTPUT);
  pinMode(pumpRelay, OUTPUT);
  pinMode(lampRelay, OUTPUT);
  pinMode(dht11, INPUT);
  pinMode(lightSensor, INPUT);
  pinMode(solutionTemp, INPUT);
  pinMode(waterSensor, INPUT);
  dht.begin();
  lcd.init();                       // initialize display
  lcd.backlight();                  // connect backlight
}

void loop() {
  userGreeting();

  if ((long)millis() - periodTimer > periodTime) {
      periodTimer = millis();   // "сбросить" таймер периода
      workTimer = millis();     // сбросить таймер выполнения
      workFlag = true;          // начали выполнение
      // включить лампу, помпу, реле, что угодно
      // банально digitalWrite(пин, HIGH)
      digitalWrite(lampRelay, HIGH);
    }
    if (((long)millis() - workTimer > workTime) && workFlag) {
      workFlag = false;            // сброс флага на выполнение
      // можно сбросить таймер периода ПОСЛЕ выполнения задачи. Подумайте над этим!
      if (timerStart) periodTimer = millis();
      // выключить лампу, помпу, реле, что угодно
      // банально digitalWrite(пин, LOW)
      digitalWrite(lampRelay, LOW);
    }
    if (workFlag) {
      // а вот этот блок кода выполняется всегда, пока мы находимся по времени "внутри" WORK_TIME
      normalModeLed();
    }

  normalModeLed();
  lightSensorWork();
  solutionTempSensorWork();
  tempAndHumidSensorWork();
  waterLevelSensorWork();
}

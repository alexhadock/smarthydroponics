// Used sensor - LM393

const int pinA0 = A0; // pin declaration

int readA0;

void setup() {
   Serial.begin(9600);
   pinMode (pinA0, INPUT);              // install of a pin A0 as input
}

void loop()
{
  readA0 = analogRead (pinA0);         // Read value from pin A0
  Serial.print("IN = ");            // Write text
  Serial.println (readA0, DEC);     // print value from "readA0"

   delay (500);

}

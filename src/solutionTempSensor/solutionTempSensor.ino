#include <OneWire.h>

#define DS 11
OneWire  ds1(DS);

void setup() {
 Serial.begin(9600);
}

void loop() {
  byte data1[2];

  ds1.reset();
  ds1.write(0xCC);
  ds1.write(0x44);

// Читаем данные с ds18b20
  ds1.reset();
  ds1.write(0xCC);
  ds1.write(0xBE);
  data1[0] = ds1.read();
  data1[1] = ds1.read();

  float TempDS18B20 = ((data1[1] << 8) | data1[0]) * 0.0625;

  Serial.println(TempDS18B20);

  delay(1000);
}

// Red color - recovery mode
// Green color - normal mode
// Blue color - economy mode

// pin numbers
const int pinR = 12;
const int pinG = 10;
const int pinB = 9;

void setup() {
  // change mode
  pinMode(pinR, OUTPUT);
  pinMode(pinG, OUTPUT);
  pinMode(pinB, OUTPUT);
}

void normalMode() {
  digitalWrite(pinG, HIGH);
}

void economyMode() {
  digitalWrite(pinB, HIGH);
}

void recoveryMode() {
  digitalWrite(pinR, HIGH);
  delay(750);
  digitalWrite(pinR, LOW);
  delay(750);
}

void loop() {
 
}

#include "DHT.h"

#define DHTPIN 2

//DHT dht(DHTPIN, DHT22); // uncomment this line if you use the DHT22 sensor
DHT dht(DHTPIN, DHT11);

void setup() {
  pinMode(2, INPUT);
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  delay(2000);
  float humidity = dht.readHumidity(); // measure humidity
  float temp = dht.readTemperature(); // meuasure temperature

  if (isnan(humidity) || isnan(temp)) {  // if it does not turn out to consider indications, the error will be displayed
    Serial.println("Reading error!");
    return;
  }

  // print values

  Serial.print("Humidity: ");
  Serial.print(humidity);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(temp);
  Serial.println(" *C ");
}

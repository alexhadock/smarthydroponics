#include <Wire.h>
#include <LiquidCrystal_I2C.h> // connect lib

LiquidCrystal_I2C lcd(0x27,16,2); //

int temperature;
int humidity;

void setup() {
  lcd.init();                       // initialize display
  lcd.backlight();                  // connect backlight
}

void userGreeting() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("SmartHydroponics");
  lcd.setCursor(0,1);
  lcd.print("@alexhadock");
}

// function for print temperature
void printTemp() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Temperature");
  lcd.setCursor(0,1);
  lcd.print(temperature);
}

// function for print humidity
void printHumid() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Humidity");
  lcd.setCursor(0,1);
  lcd.print(humidity);
}

void loop() {
  userGreeting();
  delay(2000);
  printTemp();
  delay(5000);
  printHumid();
  delay(5000);
}

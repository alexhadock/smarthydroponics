int lvl;

void setup() {
  pinMode(A0, INPUT);
  Serial.begin(9600);
}

void loop() {
  lvl = analogRead(A0);
  Serial.print("Water level = ");
  Serial.print(lvl);
  delay(1000);
}

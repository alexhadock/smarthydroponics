
unsigned long lampPeriodTime = (long)3000;  // frequency of inclusion of lamps (ms)
unsigned long pumpPeriodTime = (long)3000; // frequency of inclusion of pump (ms)

unsigned long lampWorkTime = 1000;  // operating time of lamps (ms)
unsigned long pumpWorkTime = 1000;  // operating time of lamps (ms)

#define pumpRelay 7
#define lampRelay 8
#define lampTimerStart 0    // 1 - counting of the period from the moment of SWITCHING OFF of a LAMP, 0 - from SWITCHING ON
#define pumpTimerStart 0    // 1 - counting of the period from the moment of SWITCHING OFF of a PUMP, 0 - from SWITCHING ON

unsigned long lampPeriodTimer, lampWorkTimer;
unsigned long pumpPeriodTimer, pumpWorkTimer;

boolean lampWorkFlag;
boolean pumpWorkFlag;

void setup() {
  pinMode(pumpRelay, OUTPUT);
  pinMode(lampRelay, OUTPUT);
  lampPeriodTimer = millis(); // reset timer
  pumpPeriodTimer = millis();
}

void loop() {
  if ((long)millis() - lampPeriodTimer > lampPeriodTime) {
      lampPeriodTimer = millis();   // reset period timer
      lampWorkTimer = millis();     // reset work timer
      lampWorkFlag = true;          // execution begins
      // lamp ON
      digitalWrite(lampRelay, HIGH);
    }
    if (((long)millis() - lampWorkTimer > lampWorkTime) && lampWorkFlag) {
      lampWorkFlag = false;            // reset flag
      if (lampTimerStart) {
        lampPeriodTimer = millis();
        digitalWrite(lampRelay, LOW);
      }
    }
    if (lampWorkFlag) {
      // this code is carried out always while the lamp burns
    }

    if ((long)millis() - pumpPeriodTimer > pumpPeriodTime) {
        pumpPeriodTimer = millis();   // reset period timer
        pumpWorkTimer = millis();     // reset work timer
        pumpWorkFlag = true;          // execution begins
        // lamp ON
        digitalWrite(pumpRelay, HIGH);
      }
      if (((long)millis() - pumpWorkTimer > pumpWorkTime) && pumpWorkFlag) {
        pumpWorkFlag = false;            // reset flag
        if (pumpTimerStart) {
          pumpPeriodTimer = millis();
          digitalWrite(pumpRelay, LOW);
        }
      }
      if (pumpWorkFlag) {

      }

}

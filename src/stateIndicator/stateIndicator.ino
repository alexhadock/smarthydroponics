// Red color - recovery mode
// Green color - normal mode
// Blue color - economy mode

  // pin numbers
const int pinR = 9;
const int pinG = 10;
const int pinB = 11;

void setup() {
  // change mode
  pinMode(pinR, OUTPUT);
  pinMode(pinG, OUTPUT);
  pinMode(pinB, OUTPUT);
}

void normalMode() {
  // the LED will burn with green
  digitalWrite(pinR, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinG, HIGH); 
}

void economyMode() {
  // the LED will burn with blue
  digitalWrite(pinR, LOW);
  digitalWrite(pinG, LOW);
  digitalWrite(pinB, HIGH); 
}
  // the LED will burn with red, blinking
void recoveryMode() {
  digitalWrite(pinB, LOW);
  digitalWrite(pinG, LOW);
  digitalWrite(pinR, HIGH);
  delay(750);
  digitalWrite(pinR, LOW);
  delay(750);
}

void loop() {
  normalMode();
  delay(2000);
  economyMode();
  delay(2000);
  recoveryMode();
  delay(2000);
}
